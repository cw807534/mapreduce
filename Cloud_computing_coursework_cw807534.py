import csv
import multiprocessing as mp

def map_passenger(record):
    """
    Map phase: Function to map each passenger to a count of 1.
    
    Args:
        record (list): A record representing a passenger.
        
    Returns:
        tuple: A tuple containing the passenger name and count of 1.
    """
    try:
        passenger = record[0]  # Extract passenger name
        return (passenger, 1)  # Return tuple of passenger name and count 1
    except ValueError:
        return None

def shuffle(mapped_output):
    """
    Shuffle phase: Organize mapped data by passenger name.
    
    Args:
        mapped_output (list): List of tuples containing passenger names and counts.
        
    Returns:
        dict: Dictionary where keys are passenger names and values are lists of counts.
    """
    shuffled_data = {}  # Initialize empty dictionary for shuffled data
    for item in mapped_output:
        if item:
            passenger, count = item  # Extract passenger name and count
            if passenger not in shuffled_data:
                shuffled_data[passenger] = [count]  # Initialize list for new passenger
            else:
                shuffled_data[passenger].append(count)  # Append count to existing list
    return shuffled_data

def reduce_counts(passenger_counts):
    """
    Reduce phase: Aggregate counts for each passenger.
    
    Args:
        passenger_counts (tuple): Tuple containing passenger name and list of counts.
        
    Returns:
        tuple: Tuple containing passenger name and sum of counts.
    """
    passenger, counts = passenger_counts  # Unpack tuple into passenger name and list of counts
    return passenger, sum(counts)  # Return tuple with passenger name and sum of counts

if __name__ == "__main__":
    input_filename = 'D:/Download/AComp_Passenger_data_no_error.csv'  # Path to input CSV file
    
    # Read input data from CSV file
    with open(input_filename, 'r', encoding='utf-8') as file:
        input_data = list(csv.reader(file, delimiter=','))  # Parse CSV data into list of records
    
    # Map phase
    with mp.Pool(processes=mp.cpu_count()) as pool:
        mapped_output = pool.map(map_passenger, input_data)  # Apply map function to each record
    
    # Shuffle phase
    shuffled_data = shuffle(mapped_output)  # Organize mapped data by passenger name
    print("\nShuffle Phase Output:", shuffled_data)
    
    # Reduce phase
    with mp.Pool(processes=mp.cpu_count()) as pool:
        reduced_output = pool.map(reduce_counts, shuffled_data.items())  # Aggregate counts for each passenger

    print(reduced_output)  # Print the reduced output
